// Author: Thorbjoern
// Small program that calculates the remaining work hours

#include<iostream>
#include<cmath>
#include<ctime>

int main()
{
    std::time_t t = std::time(0);           // Get the time
    std::tm* now = std::localtime(&t);      // The local calendar time
    int day = now->tm_wday;          // Extract the relevant data
    int hours = now->tm_hour;
    int minutes = now->tm_min;
    // Assign values for the work week. 8-16 Mon-Fri.
    int workDays = 5;
    int dayHours = 8;
    int minHours = 60;
    int workStart = 8;
    int workStop = 16;

    int hoursLeft = 0;
    int minutesLeft = 0;

    // Check if in the weekdays
    if ((day>0) && (day<6))
    {
        // 8 hours per day
        hoursLeft = (workDays-day)*dayHours;
        // Check if the current work has started
        if(hours < workStop)
        {
            if(hours>=workStart)
            {
                hoursLeft += dayHours - (hours-workStart+1);
                minutesLeft = minHours -minutes;
            }
            else
            {
                hoursLeft += 8;
            }
        }
    }

    std::cout<<"Work hours remaining in this week: ";
    std::cout<<hoursLeft<<" Hours and "<<minutesLeft<<" Minutes"<<std::endl;

    return 0;
}